---
kind: StatefulSet
apiVersion: {{ include "capabilities.statefulset.apiVersion" . }}
metadata:
  name: {{ include "names.fullname" . }}
  labels:
    {{- include "labels.standard" . | nindent 4 }}
spec:
  selector:
    matchLabels:
      {{- include "labels.matchLabels" . | nindent 6 }}
  serviceName: {{ include "names.fullname" . }}
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
        {{- include "labels.matchLabels" . | nindent 8 }}
    spec:
    {{- with (concat .Values.imagePullSecrets .Values.global.imagePullSecrets) }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{ toYaml . | nindent 8 | trim }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{ toYaml . | nindent 8 | trim }}
    {{- end }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{ toYaml . | nindent 8 | trim }}
    {{- end }}
    {{- with .Values.securityContext }}
      securityContext:
        {{ toYaml . | nindent 8 | trim }}
    {{- end }}
      serviceAccountName: {{ include "names.serviceAccountName" . }}
      priorityClassName: {{ .Values.priorityClassName | quote }}
      {{- if .Values.persistence.enabled }}
      containers:
        - name: "client"
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ["geth"]
          args:
          {{- if or .Values.global.rpc.enabled .Values.rpc.enabled }}
            - "--http"
            - "--http.addr=0.0.0.0"
            - "--http.port={{ coalesce .Values.global.rpc.port .Values.rpc.port }}"
            - "--http.corsdomain={{ coalesce .Values.global.rpc.corsDomain .Values.rpc.corsDomain }}"
            - "--http.vhosts={{ coalesce .Values.global.rpc.vhosts .Values.rpc.vhosts }}"
            - "--http.api={{ coalesce .Values.global.rpc.api .Values.rpc.api }}"
          {{- end }}
          {{- if or .Values.global.ws.enabled .Values.ws.enabled }}
            - "--ws"
            - "--ws.addr=0.0.0.0"
            - "--ws.port={{ coalesce .Values.global.ws.port .Values.ws.port }}"
            - "--ws.origins={{ coalesce .Values.global.ws.origins .Values.ws.origins }}"
            - "--ws.api={{ coalesce .Values.global.ws.api .Values.ws.api }}"
          {{- end }}
            - "--datadir=/data/q"
            - "--ipcdisable"
            - "--networkid={{ coalesce .Values.global.network .Values.network }}"
            - "--nat=extip:$(POD_IP)"
            - "--port=30303"
            - "--txpool.pricelimit=47619047619"
            - "--rpc.allow-unprotected-txs"
            - "--syncmode={{ .Values.syncmode }}"
          {{- if .Values.miner }}
            {{- if .Values.miner.enabled }}
            - "--mine"
            {{- end }}
            - "--miner.gasprice=50000000000"
            - "--unlock={{ .Values.miner.account }}"
            - "--keystore=/keystore"
            - "--password=/keystore/pwd.txt"
          {{- end }}
          {{- if .Values.root }}
            - "--root.addresses={{ .Values.root.addresses }}"
            - "--root.timestamp={{ .Values.root.timestamp }}"
          {{- end }}
          {{- range .Values.extraFlags }}
            - {{ . | quote }}
          {{- end }}
          {{- if or .Values.global.metrics.enabled .Values.metrics.enabled }}
          {{- range .Values.metrics.flags }}
            - {{ . | quote }}
          {{- end }}
          {{- end }}
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
          ports:
          {{- if or .Values.global.rpc.enabled .Values.rpc.enabled }}
            - name: "rpc"
              containerPort: {{ coalesce .Values.global.rpc.port .Values.rpc.port }}
          {{- end }}
          {{- if or .Values.global.ws.enabled .Values.ws.enabled }}
            - name: "ws"
              containerPort: {{ coalesce .Values.global.ws.port .Values.ws.port }}
          {{- end }}
          {{- if or .Values.global.metrics.enabled .Values.metrics.enabled }}
            - name: "metrics"
              containerPort: {{ .Values.metrics.port }}
          {{- end }}
          volumeMounts:
            - name: "data"
              mountPath: "/data"
            - name: "genesis"
              mountPath: "/config"
            {{ if .Values.miner.keystore }}
            - name: "keystore-file"
              mountPath: "keystore/{{ .Values.miner.keystore.name }}"
              subPath: {{ .Values.miner.keystore.name }}
            - name: "keystore-pwd"
              mountPath: "keystore/pwd.txt"
              subPath: "pwd.txt"
            {{- end }}
        {{- with .Values.resources }}
          resources:
            {{ toYaml . | nindent 12 | trim }}
        {{- end }}
      volumes:
        - name: "genesis"
          configMap:
            name: {{ include "names.fullname" . }}
        {{ if .Values.miner.keystore }}
        - name: "keystore-file"
          secret:
            secretName: {{ include "names.fullname" . }}
            items:
              - key: {{ .Values.miner.keystore.name }}
                path: {{ .Values.miner.keystore.name }}
        - name: "keystore-pwd"
          secret:
            secretName: {{ include "names.fullname" . }}
            items:
              - key: "pwd.txt"
                path: "pwd.txt"
        {{- end }}

{{- if (not .Values.persistence.enabled) }}
        - name: data
          emptyDir: {}
{{- else }}
  volumeClaimTemplates:
    - metadata:
        name: "data"
        labels:
          {{- include "labels.matchLabels" . | nindent 10 }}
      {{- with .Values.persistence.annotations }}
        annotations:
          {{ toYaml . | nindent 10 | trim }}
      {{- end }}
      spec:
        accessModes: {{ .Values.persistence.accessModes }}
        storageClassName: {{ .Values.persistence.storageClassName }}
        resources:
          requests:
            storage: {{ .Values.persistence.size | quote }}
{{- end }}
